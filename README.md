<h1>Assignment 2: Buffer Manager.</h1>

<hr>

<b>GROUP 17</b> <br>
<b>Team Members </b> <br>
1. Ramya Konkala  (A20497022).<br>
2. Vaibhav Patil  (A20492152).<br>
3. Saipavan Kunda (A20496516).<br>

<hr>

<h3> Aim: </h3>

The goal of this assignment is to implement a simple buffer manager. The buffer manager manages a fixed number of pages in memory that represent pages from a page file managed by the storage manager.

<hr>

<h3>Project Files</h3>

<b>C Source Files </b> </br> 
dberror.c <br>
storage_mgr.c <br>
buffer_mgr.c <br>
test_assign2_1.c <br>
test_assign2_2.c <br>



<br>
<b>Header Files </b> </br> 
dberror.h <br>
storage_mgr.h <br>
buffer_mgr.h <br>
buffer_mgr_stat.h <br>
test_helper.h <br>
dt.h <br>
<br>
<b>Run File</b><br>
Makefile <br>

<hr>

<h3>Instructions to run the code.</h3>
Step 1-  <b>$ make all </b><br>
Step 2-  <b>$ ./test_assign2_1</b> for running the given test cases.<br>
Step 3 -  <b>$ ./test_assign2_2</b> for running additional test cases.<br>

Use clean command to delete all object files.
<b>$ make clean</b> <br>
<hr>

<h3>Functions Used. </h3>

In addition to the given data structures <i>BM_BufferPool</i> and <i>BM_PageHandle</i>, we have also defined 3 more data structures <i>BM_PoolTable</i>, <i>BM_Metadata</i> and <i>BM_Frames</i> to keep track of the frames,the pages in the table and the records in them respectively.

<i><b>1. Buffer Pool Functions.</b></i><br>
<br>
<b>initBufferPool():</b><br>
      - creates a new buffer pool with numPages page frames using the page replacement strategy. The pool is used to cache pages from the page file with name pageFileName.<br>



<b>shutdownBufferPool():</b>	
The buffer pool that was formed in memory is destroyed by this method. <br>
-It frees up all of the resources/memory space that the Buffer Manager is using for the buffer pool.<br> 
-Prior to removing the buffer pool, we call forceFlushPool(...), which sends all dirty pages (modified pages) to disk.<br> 

<b>forceFlushPool():</b><br>
This function writes all the dirty pages (modified pages whose dirtyBit = 1) to the disk.

<br>
<i><b>2. Page Management Functions.</b></i><br>
<br>
The page management functions load pages from disk into the buffer pool (pin pages), remove a page frame from the buffer pool (unpin page), flag a page as unclean, and cause a page fram to be written to disk.<br><br>
<b>pinPage():</b><br>
-pageNum is the page number that is pinned. It is the responsibility of the buffer manager to set the pageNum field of the page handle given to the method.


<b>unpinPage():</b>	<br>
The function unpins a page decided by the page number attribute - pageNum. <br><br>
<b>makeDirty():</b><br>
This function writes all the dirty pages (modified pages whose dirtyBit = 1) to the disk. <br>
The dirtyBit of the provided page frame is set to 1 by this function. 
It finds the page frame via pageNum by repeatedly checking each page in the buffer pool, and when the page is found, <b>dirtyBit = 1</b> is set for that page.

<b>forcePage():</b><br>
This page writes the content of the specified page frame to the page file present on disk.<br>
<br>
<i><b>3. Statistics Functions.</b></i><br>
<br>
The statistics-related functions are used to collect data about the buffer pool. As a result, it gives a variety of statistical data regarding the buffer pool.<br><br>
<b>getFramesContents():</b>	<br>
This function returns a list of PageNumbers as an array. The array size equals the buffer size (numPages).<br> 
We just iterate through all of the page frames in the buffer pool to obtain the pageNum value of the page frames in the buffer pool. 
The page number of the page contained in the "n"th page frame is the "n"th element. <br><br>
<b>getDirtyFlags():</b><br>
This function returns an array of booleans. The array size = buffer size (numPages).<br>


<b>getFixCounts():</b><br>
function returns an array of ints (of size numPages) where the ith element is
the fix count of the page stored in the ith page frame.<br>


<b>getNumReadIO():</b><br>
This function returns the total number of IO reads executed by the buffer pool, i.e. the number of disk pages read..<br>


<b>getNumWriteIO():</b><br>
-This function returns the total number of IO writes executed by the buffer pool, i.e. the number of disk pages written.<BR> 
-The writeCount variable is used to keep track of this information. When the buffer pool is created, we set writeCount to 0 and increment it whenever a page frame is written to disk.<BR>

<HR>









